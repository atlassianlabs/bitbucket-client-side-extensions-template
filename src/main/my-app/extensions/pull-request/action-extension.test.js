import extension from './action-extension.js';

describe('action extension', () => {
    it('should register  extension', () => {
        const attributes = extension();

        expect(attributes).toMatchInlineSnapshot(`
            {
              "hidden": false,
              "label": "My pull request action extension",
              "onAction": [Function],
              "type": "button",
            }
        `);
    });
});
